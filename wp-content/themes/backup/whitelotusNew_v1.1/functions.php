<?php
function enqueue_styles() {
	wp_enqueue_style( 'whiteslotus-style', get_stylesheet_uri());
	wp_register_style('font-style', 'http://fonts.googleapis.com/css?family=Oswald:400,300');
	wp_enqueue_style( 'font-style');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

function enqueue_scripts () {
	wp_register_script('html5-shim', 'http://html5shim.googlecode.com/svn/trunk/html5.js');
	wp_enqueue_script('html5-shim');
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');

if (function_exists('add_theme_support')) {
	add_theme_support('menus');
}
	
// [LoadContent link="google.com"]
function loadPageContent( $atts ) 
{
    $scAtts = shortcode_atts( array(
        'link' => 'null',
    ), $atts );

    $htmlcontent = file_get_contents($scAtts['link']);
	$htmlcontent = iconv('windows-1251', 'UTF-8', $htmlcontent);
	
	//Working variant
	/*$pos = strpos($htmlcontent, '<div id="pagecontent">');
    $htmlcontent = substr($htmlcontent, $pos);
    $pos = strpos($htmlcontent, '</div>
</div>
<div id="bancont" style="border-top: 1px solid #c3c3c3; padding: 5px 20px; text-align: center; position: relative;">');
    $htmlcontent = substr($htmlcontent, 0, $pos);
	echo $htmlcontent;*/
	
	$pos = strpos($htmlcontent, '<table border="0"><tr>
					<td style="vertical-align: top; padding: 0 20px 0 0;">');
    $htmlcontent = substr($htmlcontent, $pos);
    $pos = strpos($htmlcontent, '<br><br>





	<br>
	<div style="font-size: 12px; text-align: right;">');
    $htmlcontent = substr($htmlcontent, 0, $pos);
	
	/*$htmlcontent = str_replace('<img style="border: 3px solid #eee; border-radius: 10px;" width="200px" src="photo.php?f=461">',
	'<img style="border: 3px solid #eee; border-radius: 10px;" width="200px" src="http://omgtu.ru/ecab/persons/photo.php?f=461">',$htmlcontent);*/
	$htmlcontent = str_replace('<img style="border: 3px solid #eee; border-radius: 10px;" width="200px" src="',
	'<img style="border: 3px solid #eee; border-radius: 10px;" width="200px" src="http://omgtu.ru/ecab/persons/',$htmlcontent);
	
	
	echo $htmlcontent;
	
}
add_shortcode( 'LoadContent', 'loadPageContent' );

function additional_mime_types( $mimes ) {
	$mimes['rar'] = 'application/x-rar-compressed';
	$mimes['zip'] = 'application/zip';
	return $mimes;
}
add_filter( 'upload_mimes', 'additional_mime_types' );

<?php get_header();?>

<?php get_sidebar();?>
<section>
<div class="main-heading">
	<h1 class="sec-title"><?php the_title(); ?></h1>
</div>
<div class="post-container">
	<?php 

		$query = new WP_Query( array( 'category_name' => 'news' ) );
		
		while ( $query->have_posts() ) {
			$query->the_post();
			?>
			<div class="post-block">
				<h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		        
			
				<?php the_excerpt();	?>
			    <div class="date">	<?php the_date();	?> </div>
			
			</div>
			<?php
		}
?>
</div>
</section>
<?php get_footer(); ?>
<?php get_header();?>

<?php get_sidebar();?>
<section>
	<div class="main-heading">
	<h1><?php the_title(); ?></h1>
	</div>
	<?php while (have_posts()): the_post();?>
	<?php 
	echo "Authors: ";
	the_author_posts_link();
		
	?>
		<?php the_content();?>
		<?php
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		?>
		
	
	<?php endwhile; ?>
</section>
<?php get_footer(); ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<!DOCTYPE html>
<html>
<head>
	<meta charser="utf-8">
	<meta name="viewport" content="width=devicewidth, initialscale=1.0">
	<title>Кафедра Информатика и Вычислительная техника.</title>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/bootstrap/css/font-awesome.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style-landing.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</head>
<body>
	<div class="wrapper">
		<div id="header">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-2" id="menu">
						<h3 id="scrollAbout"><a href="#">О кафедре</a></h3>
						<h3 id="scrollEdu"><a href="#">Обучение</a></h3>
						<h3 id="scrollBase"><a href="#">Базовая кафедра</a></h3>
						<h3 id="scrollAbit"><a href="#">Поступающим</a></h3>
						<h3 id="scrollContacts"><a href="#">Контакты</a></h3>
						<h3></h3>
					</div>
					<div id="center-header" class="col-lg-8">
							<div class="row">
								<div class="col-sm-1" id="code">
									<h3>1 /*</h3>
									<h3>2</h3>
									<h3>3 */</h3>
									<h3>4</h3>
									<h3>5</h3>
									<h3>6</h3>
									<h3>7</h3>
									<h3>8</h3>
									<h3>9</h3>
									<h3>10</h3>
									<h3>11</h3>
									<h3>12</h3>
									<h3>13</h3>
									<h3>14</h3>
								</div>
								<div  class="col-sm-10 centered">
									<h3>&nbsp</h3>
									<h3>Кафедра "Информатика и Вычислительная техника"</h3>
									<h3>&nbsp</h3>
									<h3>&nbsp</h3>
									<h3>&nbsp</h3>
									<h3>&nbsp</h3>
									<h3>&nbsp</h3>
									<h3>&nbsp</h3>
									<h3>&nbsp</h3>
									<h3>&nbsp</h3>
									<h3>&nbsp</h3>
									<h3 id="scrollMore"><a href="#">| Узнать больше |</a></h3>

								</div>
								<div class="col-sm-1">
								</div>
							</div>
						
					</div>
					<div class="col-lg-2">

					</div>
					<br>
				</div>
			</div>

		</div>
		
		<div class="mentions">
			<div class="container">
				<br>
				<br>
				<br>
				<h1> От первых лиц о нашей кафедре </h1>
				<br>
				<br>
				<br>
				<div class="row-eq-height">
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-chevron-left"aria-hidden="true"></span>
					</div>
					<div class="col-lg-2">
						<img class="person-miniature" src="<?php bloginfo('template_directory'); ?>/images/landing/Potapov.png">
					</div>
					<div class="col-lg-8">
						<p>Lorem ipsum dolor sit amet. Dolore magnam aliquam quaerat voluptatem accusantium doloremque laudantium, totam rem aperiam eaque. Ipsam voluptatem, quia dolor repellendus obcaecati. Nobis est et iusto odio dignissimos ducimus, qui ratione voluptatem. Iste natus error sit voluptatem accusantium doloremque laudantium. Natus error sit voluptatem sequi. Ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam nisi. Exercitationem ullam corporis suscipit laboriosam, nisi ut et dolore magnam.</p>
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					</div>
				<div class="row-eq-height">
					<div class="col-lg-4 col-lg-offset-7 righted">
						<h2>Потапов Виктор Ильич,</h2>
						<h3 >Зав. кафедрой</h3>
					</div>
				</div>
				</div>
			</div>	
		</div>
		<div class="info">
			<div class="container">
				<h1>Обучение на нашей кафедре</h1>
				<br>
				<div class="row ">
					<div class="col-lg-5 col-lg-offset-2">
						<h1 class="lefted">Изучаемые дисциплины</h1>
					</div>
				</div>
				
				<div class="row equal">
					<div class="col-lg-2 large-icon icon-center">
						<span class="glyphicon glyphicon-book" aria-hidden="true">
					</div>
					<div class="col-lg-5">
						
						<h2>Общие предметы</h2>
						<h3>Системное программное обеспечение</h3>
						<h3>Микропроцессорные системы</h3>
						<h3>Информатика</h3>
						<h3>Архитектура ЭВМ</h3>
						<h3>Экономика</h3>
						<h3>И другое</h3>
					</div>
					<div class="col-lg-5">
						<h2>Профиль "Технологии Разработки ПО":</h2>
						<h3>Технологии программирования</h3>
						<h3>Базы данных</h3>
						<h2>Профиль "Вычислительные машины, сети, и комплексы":</h2>
						<h3>Сети и телекоммуникации</h3>
						<h3>И другие</h3>
					</div>
				</div>
				
					<h3 class="centered small-icon"><a ><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> Скачать полный учебный план</a></h4>
					<br><br><br><br><br>



				<div class="row">
					<div class="col-lg-2 large-icon icon-center" >
						<span class="glyphicon glyphicon-user aria-hidden="true""></span>
					</div>
					<div class="col-lg-10">
					<h2>Кого мы выпускаем</h2>
					<p>Бакалавр по направлению подготовки "Информатика и вычислительная техника" в соответствии с требованиями "Квалификационного справочника должностей руководителей, специалистов и других служащих", может занимать непосредственно после окончания вуза следующие должности:
					<br>- инженер;
					<br>- инженер-программист (программист);
					<br>- инженер-электроник (электроник);
					<br>- инженер по автоматизированным системам управление;
					<br>- и другие должности, соответствующие его квалификации.
					</p>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-2 large-icon icon-center">
						<span class="glyphicon glyphicon-education aria-hidden="true""></span>
					</div>
					<div class="col-lg-10">
					<h2>Базовая кафедра «ИСС АРТ» </h2>
					<p>Базовая кафедра, проводимая в компании «ИСС АРТ» предлагает обучение и научно-исследовательскую работу по следующим направлениям:
					<br>- Математическое обеспечение и администрирование информационных систем,
					<br>- Системный анализ и управление,
					<br>- Программная инженерия,
					<br>- Информатика и вычислительная техника.
					<br>В процессе обучения студенты узнают об интересных проектах, об особенностях командной разработки, широких возможностях профессионального роста молодого специалиста, познакомятся с корпоративной культурой ООО «ИСС Арт».
					</p>
					<h3><span class="glyphicon glyphicon-globe" aria-hidden="true"></span><a href="https://www.issart.com/ru/"> Сайт компании «ИСС АРТ»</a></h1>
					</div>
				</div>

			</div>
			
		</div>
		<div class="mentions">
			<div class="container">
				<br>
				<br>
				<br>
				<h1> Что думают наши выпускники о кафедре </h1>
				<br>
				<br>
				<br>
				<div class="row-eq-height">
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-chevron-left"aria-hidden="true"></span>
					</div>
					<div class="col-lg-2">
						<img class="person-miniature" src="<?php bloginfo('template_directory'); ?>/images/landing/noname.png">
					</div>
					<div class="col-lg-8">
						<p>Lorem ipsum dolor sit amet. Dolore magnam aliquam quaerat voluptatem accusantium doloremque laudantium, totam rem aperiam eaque. Ipsam voluptatem, quia dolor repellendus obcaecati. Nobis est et iusto odio dignissimos ducimus, qui ratione voluptatem. Iste natus error sit voluptatem accusantium doloremque laudantium. Natus error sit voluptatem sequi. Ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam nisi. Exercitationem ullam corporis suscipit laboriosam, nisi ut et dolore magnam.</p>
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					</div>
				<div class="row-eq-height">
					<div class="col-lg-4 col-lg-offset-7 righted">
						<h2>No name,</h2>
						<h3>Генеральный директор "Noname"</h3>
					</div>
				</div>
				</div>
			</div>	
		</div>
		<div class="info">
			<div class="container">
				<h1>Поступление</h1>
				<h1 class="warning">На направлении 66 <strong>Бюджетных мест</strong></h1>
				<div class="row">
					<div class="col-lg-2 large-icon icon-center" >
						<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
					</div>
					<div class="col-lg-10 lefted">
					<h2>Экзамены, необходимые для поступления</h2>
					<h3><strong>Математика</strong> (минимальный балл - 27)</h3>
					<h3><strong>Физика</strong> (минимальный балл - 36)</h3>
					<h3><strong>Русский язык</strong>(минимальный балл -36)</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 large-icon icon-center" >
						<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
					</div>
					<div class="col-lg-10 lefted">
					<h2>Абитуриент должен направить в вуз следующие документы</h2>
					<h3>Заявление о приеме в ВУЗ</h3>
					<h3>Оригинал или ксерокопию документов, удостоверяющих личность, гражданство</h3>
					<h3>Оригинал или ксерокопию свидетельства о результатах ЕГЭ</h3>
					<h3>Оригинал или ксерокопию документа государственного образца об образовании</h3>
					
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 large-icon icon-center" >
						<span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
					</div>
					<div class="col-lg-10 lefted">
					<h2>Приемная комиссия ОмГТУ</h2>
					<h3>ICQ: 640 803 415</h3>
					<h3>Skype: priemnaya_commisiya_omgtu</h3>
					<h3>Адрес: Пр. Мира, 11, гл.корпус, фойе, каб П-102</h3>
					<h3>Телефон/факс: +7 (3812) 72-90-55 (многоканальный)</h3>
					<h3>Электронная почта: pk@omgtu.ru</h3>

					
					</div>
				</div>
				<h3 class="centered"><span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span><a href="http://www.omgtu.ru/entrant/where_to_go/index.php"> Сайт приемной коммисии</a></h1>
			</div>
		</div>
		<div id="maps" style="width: 100%; height: 600px">
			
		</div>
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-lg-offset-3">
						<a href="http://www.omgtu.ru/" class="links">Сайт университета</a>
					</div>
					<div class="col-lg-3">
						<a href="<?php home_url(); ?>/starter" class="links">Сайт кафедры</a>
					</div>
					<div class="col-lg-3">
						<a  href="http://www.omgtu.ru/entrant/where_to_go/index.php" class="links">Сайт приемной комиссии</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript">
  var myMap;
  ymaps.ready(init); // Ожидание загрузки API с сервера Яндекса
  function init () {
    myMap = new ymaps.Map("maps", {
      center: [55.025862, 73.292618], // Координаты центра карты
      zoom: 17 // Zoom
    });
    myMap.balloon.open(myMap.getCenter(),
    {
        contentHeader: '',
        contentBody: '<div id="mapHint">'+'За дополнительной информацией обращаться по адресу:'+'<br>'+' г.Омск, пр. Мира 11, корпус ОмГТУ №1'+'<br>'+'Контактный телефон: 65-24-98'+'</div>',
    }
);
  }

</script>
<script type="<?php bloginfo('template_directory'); ?>/bootstrap/js/bootstrap.js"></script>
<script>
	$(function() {
  // при нажатии на кнопку scrollup
  $('#scrollMore').click(function() {
    // переместиться в верхнюю часть страницы
    $("html, body").animate({
      scrollTop:591
    },1000);
  })
  });
	$(function() {
  // при нажатии на кнопку scrollup
  $('#scrollAbout').click(function() {
    // переместиться в верхнюю часть страницы
    $("html, body").animate({
      scrollTop:1191
    },1000);
  })
  });
	$(function() {
  // при нажатии на кнопку scrollup
  $('#scrollEdu').click(function() {
    // переместиться в верхнюю часть страницы
    $("html, body").animate({
      scrollTop:1591
    },1000);
  })
  });
	$(function() {
  // при нажатии на кнопку scrollup
  $('#scrollBase').click(function() {
    // переместиться в верхнюю часть страницы
    $("html, body").animate({
      scrollTop:1791
    },1000);
  })
  });
	$(function() {
  // при нажатии на кнопку scrollup
  $('#scrollAbit').click(function() {
    // переместиться в верхнюю часть страницы
    $("html, body").animate({
      scrollTop:3291
    },1000);
  })
  });
	$(function() {
  // при нажатии на кнопку scrollup
  $('#scrollContacts').click(function() {
    // переместиться в верхнюю часть страницы
    $("html, body").animate({
      scrollTop:4591
    },1000);
  })
  })
</script>
</body>
</html>

<aside>
	<nav class="aside-navigation">
		<?php wp_nav_menu(array('menu' => 'aside-menu', 'menu_class' => 'aside-menu')); ?>
	</nav>
	<?php get_search_form(); ?>
	
</aside>

﻿<?php get_header();?>

<?php get_sidebar();?>
<section>
<div class="main-heading">
	<h1 class="sec-title"><?php the_title(); ?></h1>
</div>

	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Potapov-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/potapov"><h3 class="person-name">Потапов Виктор Ильич</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon"> Заведующий кафедрой</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon"> доктор технических наук, профессор, академик МАН ВШ, заслуженный деятель науки и техники РФ</p>
		<div class="links">
				<a href="<?php bloginfo('url')?>/science/articles?author=potapov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
				<a href="<?php bloginfo('url')?>/science/books?author=potapov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
				<a href="<?php bloginfo('url')?>/science/presentations?author=potapov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
		</div>
	</div>
	
	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Shafeeva-206x300.jpg" > 
		<a href="<?php bloginfo('url')?>/shafeeva"><h3 class="person-name">Шафеева Ольга Павловна</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon"> Зам. заведующего кафедрой, доцент, кандидат технических наук</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon"> </p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=shafeeva">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=shafeeva">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
			
			
				<a href="<?php bloginfo('url')?>/science/presentations?author=shafeeva">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
			

		</div>
	</div>
	
	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Florensov-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/florensov"><h3 class="person-name">Флоренсов Александр Николаевич</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon"> кандидат технических наук, доцент</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon"> Читает курсы по операционным системам, компиляторам, программированию под различные операционные системы.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=florensov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=florensov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
			
			
				<a href="<?php bloginfo('url')?>/science/presentations?author=florensov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
			

		</div>
	</div>
	
	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Gumenyuk-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/gumenyk"><h3 class="person-name">Гуменюк Александр Степанович</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon"> кандидат технических наук, доцент</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon"> Читает авторский курс по теории информации. Руководит магистрантами и аспирантами по тематике анализа данных.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=gumenyk">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=gumenyk">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
			
			
				<a href="<?php bloginfo('url')?>/science/presentations?author=gumenyk">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
			

		</div>
	</div>
	
	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Kalekin-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/kalekin"><h3 class="person-name">Калекин Дмитрий Вячеславович</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon"> кандидат технических наук, доцент кафедры</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon"> Ведет теорию автоматического управления, технологию программирования. Занимается математическим моделированием рабочих процессов (термодинамика).</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=kalekin">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=kalekin">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
			
			
				<a href="<?php bloginfo('url')?>/science/presentations?author=kalekin">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
			

		</div>
	</div>
	
	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Nesteruk-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/nesteruk"><h3 class="person-name">Нестерук Валерий Филиппович</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon"> кандидат технических наук, доцент</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon"> Читает лекции по архитектуре ЭВМ и микропроцессорным системам, ведет занятия по программированию микроконтроллеров.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=nesteruk">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=nesteruk">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
			
			
				<a href="<?php bloginfo('url')?>/science/presentations?author=Nesteruk">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
			

		</div>
	</div>
	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Gricai-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/gritsay"><h3 class="person-name">Грицай Александр Сергеевич</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon"> старший преподаватель</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon"> Ведет занятия по разработке для мобильных устройств на соответствующих операционных системах.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=gritsay">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=gritsay">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
			
			
				<a href="<?php bloginfo('url')?>/science/presentations?author=gritsay">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
			

		</div>
	</div>


	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Zagorodnikov.jpg" > 
		<a href="<?php bloginfo('url')?>/zagorodnikov"><h3 class="person-name">Загородников Антон Павлович</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon">Кандидат технических наук, старший преподаватель.</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon">Ведет занятия по технологии программирования и разработки программного обеспечения.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=zagorodnikov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=zagorodnikov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
			
			
				<a href="<?php bloginfo('url')?>/science/presentations?author=zagorodnikov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
			

		</div>
	</div>

	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Surikov.png" > 
		<a href="#"><h3 class="person-name">Суриков Роман Олегович</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon">Ассистент.</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon">Ученая степень и звание: - 
Общий стаж работы/по специальности (лет): 4/4</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=surikov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=surikov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
			
			
				<a href="<?php bloginfo('url')?>/science/presentations?author=surikov">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
			

		</div>
	</div>


	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Chervenchuk.jpg" > 
		<a href="<?php bloginfo('url')?>/chervenchuk"><h3 class="person-name">Червенчук Игорь Владимирович</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon">Кандидат технических наук, доцент.</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon">Читает курсы лекций и ведет практические занятия по схемотехнике и электронике.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=Chervenchuk">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=Chervenchuk">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
			
			
				<a href="<?php bloginfo('url')?>/science/presentations?author=Chervenchuk">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
			

		</div>
	</div>

	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Spartak.jpg" > 
		<a href="#"><h3 class="person-name">Дорошенко Марина Спартаковна</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon">Старший преподаватель.
</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon">Ведет занятия по базам данных и программированию.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=#">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/article_icon.png" alt="icon">
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=#">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/book_icon.png" alt="icon">
				Книги</a>
			
			
				<a href="<?php bloginfo('url')?>/science/presentations?author=#">
				<img class="icon" src="<?php bloginfo('template_url'); ?>/images/presentation_icon.png" alt="icon">
				Презентации</a>
			

		</div>
	</div>
	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Ralovec.jpg" > 
		<a href="#"><h3 class="person-name">РАЛОВЕЦ МАРИНА ЛЕОНИДОВНА</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon">Секретарь кафедры
</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon">Помогает студентам и преподавателям кафедры по всем организационным вопросам.</p>
		<div class="links">


		</div>
	</div>
	<div class="person-block">
		<img class="collective" src="http://d9620389.bget.ru/wp-content/uploads/2017/03/Kononova.jpg" > 
		<a href="#"><h3 class="person-name">КОНОНОВА ВИКТОРИЯ ВЛАДИМИРОВНА</h3></a>
		<h3><img class="icon" src="<?php bloginfo('template_url'); ?>/images/title_icon.png" alt="icon">Методист
</h3>
		<p><img class="icon" src="<?php bloginfo('template_url'); ?>/images/case_icon.png" alt="icon">Оформляет документацию, согласовывает учебные программы, темы выпускных работ и многое другое.</p>
		<div class="links">


		</div>
	</div>
</section>
<?php get_footer(); ?>
<?php get_header();?>

<?php get_sidebar();?>
<section>
	<div class="main-heading">
	<h1><?php the_title(); ?></h1>
	</div>
	<?php while (have_posts()): the_post();?>
	<?php 
	echo "Авторы: ";
	the_author_posts_link();
		
	?>
		<?php the_content();?>
		<br>
		<?php
			$file = get_field('attached_file');
			if($file) {
		?>
				<img class="icon" src="<?php echo $file['icon']; ?>"></img>
				<a href="<?php echo $file['url']; ?>"> Загрузить файл с работой</a>
		<?php
			}
		?>

	<?php endwhile; ?>
	


  
    	<?php 
			$images = get_field('gallery');
			$size = 'medium';
			$first = true;
			if($images) { ?>
				<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
   				 <!-- Wrapper for slides -->
    			<div class="carousel-inner"> <?php

				foreach ($images as $image) { ?>
					<div class="item <?php if($first) echo 'active'; ?>">
						<img src="<?php echo $image['url']; ?>" height="100%" weight="100%" alt="<?php echo $image['alt']; ?>" />
						
					</div>
				<?php
					$first = false;
				} ?>
				</div>
				<!-- Left and right controls -->
				    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
				      <span class="glyphicon glyphicon-chevron-left"></span>
				      <span class="sr-only">Previous</span>
				    </a>
				    <a class="right carousel-control" href="#myCarousel" data-slide="next">
				      <span class="glyphicon glyphicon-chevron-right"></span>
				      <span class="sr-only">Next</span>
				    </a>
				  </div>
	<?php	} ?>
      
    
      
  
    

    




</section>
<?php get_footer(); ?>
﻿<?php get_header();?>

<?php get_sidebar();?>
<section>
<div class="main-heading">
	<h1 class="sec-title"><?php the_title(); ?></h1>
</div>

	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Potapov-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/potapov"><h3 class="person-name">Потапов Виктор Ильич</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span> Заведующий кафедрой</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> доктор технических наук, профессор, академик МАН ВШ, заслуженный деятель науки и техники РФ.</p>
		<p>Автор более 760 печатных работ в том числе 11 монографий 26 учебных пособий. 165 изобретений в области информатики и вычислительной техники и 54 зарегистрированных в Государственном ФАП программ для ЭВМ.</p>
		<div class="links">
				<a href="<?php bloginfo('url')?>/science/articles?author=potapov">
				<span class="glyphicon glyphicon-file"></span> Статьи</a>
				<a href="<?php bloginfo('url')?>/science/books?author=potapov">
				<span class="glyphicon glyphicon-book"></span> Книги</a>
				<a href="<?php bloginfo('url')?>/science/certificates?author=potapov">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты и изобретения
				</a>
		</div>
	</div>
	
	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Shafeeva-206x300.jpg" > 
		<a href="<?php bloginfo('url')?>/shafeeva"><h3 class="person-name">Шафеева Ольга Павловна</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span>  Зам. заведующего кафедрой, доцент, кандидат технических наук</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Ведет лекционные и практические занятия по программированию на языках высокого уровня </p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=shafeeva">
				<span class="glyphicon glyphicon-file"></span>
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=shafeeva">
				<span class="glyphicon glyphicon-book"></span>
				Книги</a>

				<a href="<?php bloginfo('url')?>/science/certificates?author=shafeeva">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты и изобретения
				</a>
			

		</div>
	</div>
	
	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Florensov-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/florensov"><h3 class="person-name">Флоренсов Александр Николаевич</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span>  кандидат технических наук, доцент</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Читает курсы по операционным системам, компиляторам, программированию под различные операционные системы.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=florensov">
				<span class="glyphicon glyphicon-file"></span>
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=florensov">
				<span class="glyphicon glyphicon-book"></span>
				Книги</a>
				<a href="<?php bloginfo('url')?>/science/certificates?author=florensov">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты
				</a>
			
			
			

		</div>
	</div>
	
	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Gumenyuk-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/gumenyk"><h3 class="person-name">Гуменюк Александр Степанович</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span>  кандидат технических наук, доцент</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Читает авторский курс по теории информации. Руководит магистрантами по тематике анализа данных.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=gumenyk">
				<span class="glyphicon glyphicon-file"></span>
				Статьи</a>

			
			
				<a href="<?php bloginfo('url')?>/science/books?author=gumenyk">
				<span class="glyphicon glyphicon-book"></span>
				Книги</a>
				<a href="<?php bloginfo('url')?>/science/certificates?author=gumenyk">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты
				</a>
			
				

		</div>
	</div>
	
	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Kalekin-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/kalekin"><h3 class="person-name">Калекин Дмитрий Вячеславович</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span>  кандидат технических наук, доцент кафедры</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Ведет теорию автоматического управления, технологию программирования. Занимается математическим моделированием рабочих процессов (термодинамика).</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=kalekin">
				<span class="glyphicon glyphicon-file"></span>
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=kalekin">
				<span class="glyphicon glyphicon-book"></span>
				Книги</a>
				<a href="<?php bloginfo('url')?>/science/certificates?author=kalekin">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты
				</a>
			
			
		</div>
	</div>
	
	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Nesteruk-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/nesteruk"><h3 class="person-name">Нестерук Валерий Филиппович</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span> кандидат технических наук, доцент</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Читает лекции по архитектуре ЭВМ и микропроцессорным системам, ведет занятия по программированию микроконтроллеров.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=nesteruk">
				<span class="glyphicon glyphicon-file"></span>
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=nesteruk">
				<span class="glyphicon glyphicon-book"></span>
				Книги</a>
				<a href="<?php bloginfo('url')?>/science/certificates?author=nesteruk">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты
				</a>
			
				
		</div>
	</div>
	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Gricai-214x300.jpg" > 
		<a href="<?php bloginfo('url')?>/gritsay"><h3 class="person-name">Грицай Александр Сергеевич</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span> Cтарший преподаватель</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Ведет занятия по разработке для мобильных устройств на соответствующих операционных системах.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=gritsay">
				<span class="glyphicon glyphicon-file"></span>
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=gritsay">
				<span class="glyphicon glyphicon-book"></span>
				Книги</a>
				<a href="<?php bloginfo('url')?>/science/certificates?author=gritsay">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты
				</a>


		</div>
	</div>


	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Zagorodnikov.jpg" > 
		<a href="<?php bloginfo('url')?>/zagorodnikov"><h3 class="person-name">Загородников Антон Павлович</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span> Доцент</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Ведет занятия по технологии программирования и разработки программного обеспечения.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=zagorodnikov">
				<span class="glyphicon glyphicon-file"></span>
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=zagorodnikov">
				<span class="glyphicon glyphicon-book"></span>
				Книги</a>
				<a href="<?php bloginfo('url')?>/science/certificates?author=zagorodnikov">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты
				</a>
			
			

		</div>
	</div>


	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Chervenchuk.jpg" > 
		<a href="<?php bloginfo('url')?>/chervenchuk"><h3 class="person-name">Червенчук Игорь Владимирович</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span> Кандидат технических наук, доцент.</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Читает курсы лекций и ведет практические занятия по схемотехнике и электронике.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=Chervenchuk">
				<span class="glyphicon glyphicon-file"></span>
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=Chervenchuk">
				<span class="glyphicon glyphicon-book"></span>
				Книги</a>
				<a href="<?php bloginfo('url')?>/science/certificates?author=Chervenchuk">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты
				</a>
			
			

		</div>
	</div>

	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/11/posdnichenko.jpg" > 
		<a href="<?php bloginfo('url')?>/posdnichenko"><h3 class="person-name">Николай Николаевич Поздниченко</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span> Ассистент</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Читает курс лекций, ведет практические занятия по технологиям программирования, теория моделирования и систем искуственного интеллекта.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=posdnichenko">
				<span class="glyphicon glyphicon-file"></span>
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=posdnichenko">
				<span class="glyphicon glyphicon-book"></span>
				Книги</a>
				<a href="<?php bloginfo('url')?>/science/certificates?author=posdnichenko">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты
				</a>
			
			
			

		</div>
	</div>

	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Spartak.jpg" > 
		<a href="#"><h3 class="person-name">Дорошенко Марина Спартаковна</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span> Старший преподаватель.</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Ведет занятия по базам данных и программированию.</p>
		<div class="links">
			
				<a href="<?php bloginfo('url')?>/science/articles?author=#">
				<span class="glyphicon glyphicon-file"></span>
				Статьи</a>
			
			
				<a href="<?php bloginfo('url')?>/science/books?author=#">
				<span class="glyphicon glyphicon-book"></span>
				Книги</a>
				<a href="<?php bloginfo('url')?>/science/certificates?author=#">
				<span class="glyphicon glyphicon-list-alt"></span> Сертификаты
				</a>
			
			
			
			

		</div>
	</div>
	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Ralovec.jpg" > 
		<a href="#"><h3 class="person-name">Раловец Марина Леонидовна </h3></a>
		<h3><span class="glyphicon glyphicon-education"></span> Инженер 1 категории
</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Помогает студентам и преподавателям кафедры по всем организационным вопросам.</p>
		<div class="links">


		</div>
	</div>
	<div class="person-block">
		<img class="collective" src="<?php bloginfo('url')?>/wp-content/uploads/2017/03/Kononova.jpg" > 
		<a href="#"><h3 class="person-name">Кононова Виктория Владимировна</h3></a>
		<h3><span class="glyphicon glyphicon-education"></span> Инженер 2 категории
</h3>
		<p><span class="glyphicon glyphicon-briefcase"></span> Оформляет документацию, согласовывает учебные программы, темы выпускных работ и многое другое.</p>
		<div class="links">


		</div>
	</div>
</section>
<?php get_footer(); ?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<link rel="pingback" href="<?php home_url() ?>" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/bootstrap/css/bootstrap.min.css">
	<style type="text/css">
		#page-preloader {
		    position: fixed;
		    left: 0;
		    top: 0;
		    right: 0;
		    bottom: 0;
		    background: rgba(255,255,255,1);  
		    z-index: 100500;
		}

		#page-preloader .spinner {
		    width: 128px;
		    height: 128px;
		    position: absolute;
		    left: 45%;
		    top: 45%;
		    background: url('<?php bloginfo('template_directory'); ?>/images/landing/spinner.gif') no-repeat 50% 50%;
		    background-size:contain;
		    margin: -16px 0 0 -16px;
		}
	</style>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(window).on('load', function () {
		    var $preloader = $('#page-preloader'),
		        $spinner   = $preloader.find('.spinner');
		    $spinner.fadeOut();
		    $preloader.delay(350).fadeOut('slow');
		});
	</script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="page-preloader"><span class="spinner"></span></div>
	<div class="wrapper">
	<header>
		<a href="/"><img class="aligncenter" class="aligncenter" src="<?php bloginfo('template_url'); ?>/images/logo_omgtu.png" alt="logo"></a>
		<h5 class="sec-main-title">Факультет Информационых Технологий и Комьютерных Систем</h3>
		<h2 class="header-title"><strong><a href="/starter"><?php bloginfo('name')?></a></strong></h3>
	</header>
	
</nav>
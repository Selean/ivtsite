<?php get_header(); ?>
<?php get_sidebar(); ?>
<section>
	<h1 class="main-heading">
   <?php
   
if (is_category()):
    single_cat_title();
elseif ( is_tag() ) :
	single_tag_title();
elseif (is_year()):
    printf(__('Year: %s', 'striped'), '<span>' . get_the_date(_x('Y', 'yearly archives date format', 'striped')) . '</span>');
elseif (is_month()):
    printf(__('Month: %s', 'striped'), '<span>' . get_the_date(_x('F Y', 'monthly archives date format', 'striped')) . '</span>');
elseif (is_day()):
    printf(__('Day: %s', 'striped'), '<span>' . get_the_date() . '</span>');
elseif (is_author()):
    printf( __( 'Author: %s', 'striped' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' );
elseif (is_tax($taxonomy = 'byline', $term = '')):
	printf(__('Материалы автора'));
else :
	_e( 'Archives', 'striped' );
endif;
?>
</h1>

	<?php if (have_posts()): while (have_posts()): the_post(); ?>
				
				<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				<?php the_excerpt();?>
				<hr>
	<?php endwhile; endif; ?>

	
</section>

<?php get_footer(); ?>
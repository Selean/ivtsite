<?php get_header(); ?>

<?php get_sidebar(); ?>
<section>
	<div class="main-heading">
		<h1><?php the_title(); ?></h1>
	</div>
	<?php if (have_posts()): while (have_posts()): the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; endif; ?>
</section>

<?php get_footer(); ?>
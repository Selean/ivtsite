<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<title>Заголовок</title>
	<meta content="" name="description" />
	<meta content="" property="og:image" />
	<meta content="" property="og:description" />
	<meta content="" property="og:site_name" />
	<meta content="website" property="og:type" />

	<meta content="telephone=no" name="format-detection" />
	<meta http-equiv="x-rim-auto-match" content="none">

	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="shortcut icon" href="favicon.png" />

	<link rel="stylesheet" href="landing/libs/bootstrap/bootstrap-grid-3.3.1.min.css" />
	<link rel="stylesheet" href="landing/libs/font-awesome-4.2.0/css/font-awesome.min.css" />
	<link rel="stylesheet" href="landing/libs/fancybox/jquery.fancybox.css" />
	<link rel="stylesheet" href="landing/libs/owl-carousel/owl.carousel.css" />
	<link rel="stylesheet" href="landing/libs/countdown/jquery.countdown.css" />
	<link rel="stylesheet" href="landing/css/fonts.css" />
	<link rel="stylesheet" href="landing/css/main.css" />
	<link rel="stylesheet" href="landing/css/media.css" />
</head>
<body>
	<div class=entry></div>
	<div class=mentions-carousel>
		<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				<li data-target="#carousel-example-generic" data-slide-to="2"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="..." alt="...">
					<div class="carousel-caption">
						...
					</div>
				</div>
				<div class="item">
					<img src="..." alt="...">
					<div class="carousel-caption">
						...
					</div>
				</div>
				...
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>

	<div class="hidden"></div>
	<!-- Mandatory for Responsive Bootstrap Toolkit to operate -->
	<div class="device-xs visible-xs"></div>
	<div class="device-sm visible-sm"></div>
	<div class="device-md visible-md"></div>
	<div class="device-lg visible-lg"></div>
	<!-- end mandatory -->
	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->
	<script src="landing/libs/jquery/jquery-1.11.1.min.js"></script>
	<script src="landing/libs/jquery-mousewheel/jquery.mousewheel.min.js"></script>
	<script src="landing/libs/fancybox/jquery.fancybox.pack.js"></script>
	<script src="landing/libs/waypoints/waypoints-1.6.2.min.js"></script>
	<script src="landing/libs/scrollto/jquery.scrollTo.min.js"></script>
	<script src="landing/libs/owl-carousel/owl.carousel.min.js"></script>
	<script src="landing/libs/countdown/jquery.plugin.js"></script>
	<script src="landing/libs/countdown/jquery.countdown.min.js"></script>
	<script src="landing/libs/countdown/jquery.countdown-ru.js"></script>
	<script src="landing/libs/landing-nav/navigation.js"></script>
	<script src="landing/libs/bootstrap-toolkit/bootstrap-toolkit.min.js"></script>
	<script src="landing/libs/maskedinput/jquery.maskedinput.min.js"></script>
	<script src="landing/libs/equalheight/jquery.equalheight.js"></script>
	<script src="landing/libs/stellar/jquery.stellar.min.js"></script>
	<script src="landing/js/common.js"></script>
	<script src="landing/js/bootstrap.min.js"></script>
	<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
	<!-- Google Analytics counter --><!-- /Google Analytics counter -->
</body>
</html>
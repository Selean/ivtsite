<?php
function enqueue_styles() {
	wp_enqueue_style( 'whiteslotus-style', get_stylesheet_uri());
	wp_register_style('font-style', 'http://fonts.googleapis.com/css?family=Oswald:400,300');
	wp_enqueue_style( 'font-style');
	
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

function enqueue_scripts () {
	wp_register_script('html5-shim', 'http://html5shim.googlecode.com/svn/trunk/html5.js');
	wp_enqueue_script('html5-shim');
}
add_action('wp_enqueue_scripts', 'enqueue_scripts');


if ( ! current_user_can( 'manage_options' ) ) {
	show_admin_bar( false );
}

if (function_exists('add_theme_support')) {
	add_theme_support('menus');
}
	
// [LoadContent link="google.com"]
function loadPageContent( $atts ) 
{
	echo 'Under construction.';
    $scAtts = shortcode_atts( array(
        'link' => 'null',
    ), $atts );

    $htmlcontent = file_get_contents($scAtts['link']);
    if($htmlcontent)
    {


		$htmlcontent = iconv('windows-1251', 'UTF-8', $htmlcontent);
		//Working variant
		/*$pos = strpos($htmlcontent, '<div id="pagecontent">');
	    $htmlcontent = substr($htmlcontent, $pos);
	    $pos = strpos($htmlcontent, '</div>
	</div>
	<div id="bancont" style="border-top: 1px solid #c3c3c3; padding: 5px 20px; text-align: center; position: relative;">');
	    $htmlcontent = substr($htmlcontent, 0, $pos);
		echo $htmlcontent;*/
		
		$pos = strpos($htmlcontent, '<table border="0"><tr>
						<td style="vertical-align: top; padding: 0 20px 0 0;">');
	    $htmlcontent = substr($htmlcontent, $pos);
	    $pos = strpos($htmlcontent, '<br><br>





		<br>
		<div style="font-size: 12px; text-align: right;">');
	    $htmlcontent = substr($htmlcontent, 0, $pos);
		
		/*$htmlcontent = str_replace('<img style="border: 3px solid #eee; border-radius: 10px;" width="200px" src="photo.php?f=461">',
		'<img style="border: 3px solid #eee; border-radius: 10px;" width="200px" src="http://omgtu.ru/ecab/persons/photo.php?f=461">',$htmlcontent);*/
		$htmlcontent = str_replace('<img style="border: 3px solid #eee; border-radius: 10px;" width="200px" src="',
		'<img style="border: 3px solid #eee; border-radius: 10px;" width="200px" src="http://omgtu.ru/ecab/persons/',$htmlcontent);
		
		
		echo $htmlcontent;
	}
	
}
add_shortcode( 'LoadContent', 'loadPageContent' );

function additional_mime_types( $mimes ) {
	$mimes['rar'] = 'application/x-rar-compressed';
	$mimes['zip'] = 'application/zip';
	return $mimes;
}
add_filter( 'upload_mimes', 'additional_mime_types' );


//Byline 
function add_custom_taxonomies() {
	// Add new "bylines" taxonomy to Posts
	register_taxonomy('byline', 'post', array(
		// Hierarchical taxonomy (like categories)
		'hierarchical' => false,
		'show_admin_column' => true,
		// This array of options controls the labels displayed in the WordPress Admin UI
		'labels' => array(
			'name' => _x( 'Bylines', 'taxonomy general name' ),
			'singular_name' => _x( 'Byline', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search Bylines' ),
			'popular_items' => __('Popular Bylines'),
			'all_items' => __( 'All Bylines' ),
			'edit_item' => __( 'Edit Byline' ),
			'update_item' => __( 'Update Byline' ),
			'separate_items_with_commas' => __( 'Separate bylines with commas' ),
			'add_new_item' => __( 'Add New Byline' ),
			'add_or_remove_items' => __( 'Add or remove bylines' ),
			'choose_from_most_used' => __( 'Choose from most used bylines' ),			
			'new_item_name' => __( 'New Byline Name' ),
			'menu_name' => __( 'Bylines' )
		),
		// Control the slugs used for this taxonomy
		'rewrite' => array(
			'slug' => 'about/collective', // This controls the base slug that will display before each term
			'with_front' => true, // Don't display the category base before "/bylines/"
			'hierarchical' => false // This will allow URL's like "/bylines/boston/cambridge/"
		),
	));
}
add_action( 'init', 'add_custom_taxonomies', 0 );


//Display the byline by replacing instances of the_author throughout most areas of the site

add_filter( 'the_author', 'byline' );
add_filter( 'get_the_author_display_name', 'byline' );

function byline( $name ) {
global $post;

$author = get_the_term_list( $post->ID, 'byline', '', ', ', '' );

//if ( $author && is_singular() || is_home() || is_page() || is_category() || is_tag() )  Use other if statements to control display if desired. 
if ( $author && !is_admin() && !is_feed() )
$name = $author;

return $name;

if ( $author && is_feed() )  //Preserves native Wordpress author for feeds
$name = get_the_author();
return $name;

}


// sertificates post type;
add_action( 'init', 'create_boat' );

function create_boat() {
    register_post_type( 'sertificates',
        array(
            'labels' => array(
                'name' => 'Сертификаты',
                'singular_name' => 'Сертификат',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Sertificate',
                'edit' => 'Edit',
                'edit_item' => 'Edit Sertificate',
                'new_item' => 'New Sertificate',
                'view' => 'View',
                'view_item' => 'View Sertificate',
                'search_items' => 'Search Sertificates',
                'not_found' => 'No Sertificates found',
                'not_found_in_trash' => 'No Sertificates found in Trash',
                'parent' => 'Parent Sertificate'
            ),
            'public' => true,
            'menu_position' => 15,
            'supports' => array('title','custom-fields','featured-image' ),
            'taxonomies' => array( 'byline' ),
            'menu_icon' => 'dashicons-dashboard',
            'hierarchical' => false,
            'has_archive' => true
        )
    );
}
<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html>
<head>
	<meta charser="utf-8">
	<meta name="viewport" content="width=devicewidth, initialscale=1.0">
	<title>Кафедра Информатика и Вычислительная техника.</title>
	<style type="text/css">
		#page-preloader {
		    position: fixed;
		    left: 0;
		    top: 0;
		    right: 0;
		    bottom: 0;
		    background: rgba(255,255,255,1);  
		    z-index: 100500;
		}

		#page-preloader .spinner {
		    width: 128px;
		    height: 128px;
		    position: absolute;
		    left: 45%;
		    top: 45%;
		    background: url('<?php bloginfo('template_directory'); ?>/images/landing/spinner.gif') no-repeat 50% 50%;
		    background-size:contain;
		    margin: -16px 0 0 -16px;
		}
	</style>
	
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/bootstrap/css/font-awesome.css">
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style-landing.css">
	<link href="https://fonts.googleapis.com/css?family=Arsenal|Press+Start+2P" rel="stylesheet">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(window).on('load', function () {
		    var $preloader = $('#page-preloader'),
		        $spinner   = $preloader.find('.spinner');
		    $spinner.fadeOut();
		    $preloader.delay(350).fadeOut('slow');
		});
	</script>
</head>
<body>
	<div id="page-preloader"><span class="spinner"></span></div>
	<div class="wrapper">
		<div id="header">
			<div class="container">
				<div class="row equal">
					<div class="col-md-2 menu-label" id="menu">
						<h2 id="scrollAbout"><a href="#">О кафедре</a></h2>
						<h2 id="scrollEdu"><a href="#">Обучение</a></h2>
						<h2 id="scrollBase"><a href="#">Базовая кафедра</a></h2>
						<h2 id="scrollAbit"><a href="#">Поступающим</a></h2>
						<h2 id="scrollContacts"><a href="#">Контакты</a></h2>
						
					</div>
					<div id="center-header" class="col-md-8">

							<h1>Кафедра</h1>
							<h1>Информатика и Вычислительная техника</h1>
							<h4 class="slogan">Научим программировать твое будущее.</h1>
							<h2 id="scrollMore"><a href="#">| Узнать больше |</a></h2>

							
							
						
					</div>

					
					<br>
				</div>
			</div>

		</div>
		
		<div class="mentions">
			<div class="container">
				<br>
				<br>
				<br>
				<h1> От первых лиц о нашей кафедре </h1>

				<div class="row equal">
		        	<div class="col-lg-offset-2">
		        		<img class="person-miniature" src="<?php bloginfo('template_directory'); ?>/images/landing/Potapov.png" style="margin-top: 60%" >
		        	</div>
		        	<div class="col-lg-6">
		        		<h2>Уважаемые абитуриенты!</h2>
		        		<p class="main-mention">Перед вами стоит задача сделать одно из важнейших решений в своей жизни - выбрать будущую профессию, которая бы вас не разочаровала в дальнейшем. Имея 50-летний опыт подготовки инженеров, бакалавров, магистров и аспирантов в области информатики и вычислительной техники, думаю, что вы не ошибетесь выбрав для поступления в ОмГТУ направление 09.03.01 - "Информатика и вычислительная техника", ведущее направление развития науки и техники в XXI веке.</p>
		            	<div class="righted">
		          			<h2>Потапов Виктор Ильич,</h2>
							<h3>Зав. кафедрой</h3> 
						</div>
		        	</div>
		        </div>

			</div>	
		</div>
		<div class="info">
			<div class="container">
				<h1>Обучение на нашей кафедре</h1>
				<br>
				<div class="row">
					<div class="col-lg-offset-2">
						<h3>Кафедра ведет подготовку бакалавров, по следующим профилям подготовки:</h3>
						<ul>
							<li>Вычислительные машины, комплексы и системы связи</li>
							<li>Технологии разработки программного обеспечения</li>
							<li>Автоматизированные системы обработки информации и управления</li>
					</div>
				</div>
				
				<div class="row ">
					<div class="col-lg-offset-2">
						<h2 class="lefted">Дисциплины, изучаемые бакалаврами</h1>
					</div>
				</div>
				
				<div class="row equal">
					<div class="col-lg-2 large-icon icon-center">
						<span class="glyphicon glyphicon-book" aria-hidden="true">
					</div>
					<div class="col-lg-5">
						
						<h2>Общие предметы:</h2>
						<ul>
							<li>Информатика;</li>
							<li>Программирование;</li>
							<li>Сети и телекоммуникации;</li>
							<li>Прикладная теория цифровых автоматов;</li>
							<li>Системное программное обеспечение;</li>
							<li>Схемотехнические решения в вычислительной технике;</li>
							<li>Архитектура ЭВМ и систем;</li>
							<li>Микропроцессорные системы;</li>
							<li>Конструкторское проектирование ЭВМ;</li>
							<li>Структурный системный анализ;</li>
						</ul>
					</div>
					<div class='col-lg-5'>
						<img id="image" src="<?php bloginfo('template_directory'); ?>/images/landing/img1.jpg">
					</div>
					
				</div>
				<div class="row">
					<div class="col-lg-5 col-lg-offset-2">
						<h2>Профиль "Технологии разработки программного обеспечения":</h2>
						<ul>
							<li>Технологии разработки программного обеспечения;</li>		
							<li>Визуальное программирование и инструментальные среды;</li>
							<li>Программирование в графических и событийно-управляемых системах;</li>
							<li>Проектирование информационных систем;</li>
							<li>Web-технологии;</li>
							<li>Разработка мобильных приложений</li>
						</ul>
					</div>
					<div class="col-lg-5">
						<h2>Профиль "Вычислительные машины, комплексы, системы и сети":</h2>
						<ul>
							<li>Технологии программирования;</li>		
							<li>Системы программирования;</li>
							<li>Архитектура микроконтроллеров;</li>
							<li>Проектирование информационных систем;</li>
							<li>Разработка web-систем;</li>
						</ul>
					</div>
				</div>
				
					<h3 class="centered small-icon"><a href="http://www.omgtu.ru/educational_activities/areas-of-training-implemented-in-omsk-university-in-accordance-with-gef-in/bachelor/the-list-of-working-programs-for-information-klasternaya-section/09.03.01/09.03.01_Plan.pdf" ><span class="glyphicon glyphicon-download-alt" aria-hidden="true"> Скачать полный учебный план</a></h4>
					<br><br><br><br><br>



				<div class="row">
					<div class="col-lg-2 large-icon icon-center" >
						<span class="glyphicon glyphicon-user aria-hidden="true""></span>
					</div>
					<div class="col-lg-10">
					<h2>Кого мы выпускаем</h2>
					<h3>У нас многоуровневая система обучения, включающая:</h3>
					<ul>
						<li>Бакалавриат - (09.03.01),</li>
						<li>Магистратуру - (09.04.01),</li>
					</ul>
					<p>Наши выпускники получают широкие знания в области разработки программного обеспечения, построения высоконагруженных информационных систем, программирования мобильных устройств, владеют принципами построения архитектуры с использованием микронокроллеров, и являются универсальными специалистами, которые успешно работают в IT и бакновском секторе, а также в военно-промышленном комплексе. 
					</p>
					</div>
				</div>


				<div class="row">
					<div class="col-lg-2 large-icon icon-center">
						<span class="glyphicon glyphicon-education aria-hidden="true""></span>
					</div>
					<div class="col-lg-10">
					<h2>Базовая кафедра «ИСС АРТ» </h2>
					<p>Компания ООО "ИСС АРТ" является одной из ведущих ИТ компаний региона, осуществляет работу в области разработки программного обеспечения на российском и зарубежных рынках. Компания ООО "ИСС-АРТ" является стратегическим партнером в области разработки программного обеспечения и анализа данных – Data mining, а с 2015 года представлена в виде базовой кафедры Факультета информационных технологий и компьютерных систем кафедры «Информатика и вычислительная техника». Это позволило студентам кафедры получить практические навыки и знания в области коллективной разработки программного обеспечения, методикам разработки, таким как Agile и Scrum. Кроме того, студенты получают обширную практику и первый опыт работы с коммерческими IT – проектами.   
					</p>
					<h3><span class="glyphicon glyphicon-globe" aria-hidden="true"></span><a href="https://www.issart.com/ru/"> Сайт компании «ИСС АРТ»</a></h1>
					</div>
				</div>

			</div>
			
		</div>
		<div class="sec-mentions">
			<div class="container">
				<br>
				<br>
				<br>
				<h1> Что думают преподаватели и наши выпускники о кафедре </h1>
				<div id="myCarousel2" class="carousel slide" data-ride="carousel" data-interval="false">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">
        <img src="<?php bloginfo('template_directory'); ?>/images/carousel-bg.png" alt="Los Angeles" style="width:100%;">
        <div class="carousel-caption">
        	<div class="row equal">
        	<div class="col-lg-3">
        								<img class="person-miniature" src="<?php bloginfo('template_directory'); ?>/images/landing/sonkin1.png" style="margin-top: 60%">
        	</div>
        	<div class="col-lg-9">
        		<p>Горжусь тем, что являюсь выпускником первого набора по специальности ЭВМ Омского политехнического института. Педагогичесикй коллектив университета всегда отличался высоким профессионализмом и преданностью профессии, огромным желанием максимально передать свои знания студентам. С благодарностью вспоминааю своего наставника В.И. Потапова.</p>
            	<div class="righted">
          			<h2>М.А. Сонькин,</h2>
						<h3>Заместитель губернатора Томской области по научно-образовательного комплексу и инновационной политике.</h3>
				</div>
        	</div>
        	</div>
          
          
        </div>
      </div>
      <div class="item">
        <img src="<?php bloginfo('template_directory'); ?>/images/carousel-bg.png" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          <div class="row equal">
        	<div class="col-lg-3">
        	<img class="person-miniature" src="<?php bloginfo('template_directory'); ?>/images/landing/mehaev.png" style="margin-top: 60%">
        	</div>
        	<div class="col-lg-9">
        		<p>Обучение на кафедре "Информатика и вычислительная техника" дало мне обширную теоретическую базу в области информационных технологий. Полученные знания и навыки самостоятельного обучения, приобретенные за время учебы, позволяют уверенно осваивать и применять в работе новые технологии и инструменты, что очень важно в век стремительного развития информационных технологий.</p>
            	<div class="righted">
          			<h2>Мехаев Дмитрий Михайлович,</h2>
						<h3>Ведущий инженер-программист сектора разработки автоматизированных систем наличного денежного обращения отдела разработки и сопровождения АС Банка России.</h3> 
				</div>
        	</div>
        	</div>
        </div>
      </div>
    
      <div class="item">
        <img src="<?php bloginfo('template_directory'); ?>/images/carousel-bg.png" alt="New York" style="width:100%;">
        <div class="carousel-caption">
          <div class="row equal">
        	<div class="col-lg-3">
        				<img class="person-miniature" src="<?php bloginfo('template_directory'); ?>/images/landing/birukov.png" style="margin-top: 60%">
        	</div>
        	<div class="col-lg-9">
        		<p>
				Я вспоминаю в первую очередь мощный  преподавательский состав, в лицах В.И. Потапова, В.Ф. Нестерука, А.Н. Флоренсова, О.П. Шафеевой., А.С. Гуменюка. Они заложили в нас способность к аналитическому мышлению, структурному подходу и решению инженерных задач, - а это главная база для дальнейшего развития и приобретения новых навыков! Желаю кафедре финансового благополучия в соответствие с задумками и планами коллектива. 
				</p>
            	<div class="righted">
          			<h2>Бирюков Николай,</h2>
						<h3>Предприниматель, создатель компаний - проектов:  Спецстройсибирь.рф , Завод полиэтиленовой трубы  Пэт-Сибирь.</h3>
				</div>
        	</div>
        	</div>
        </div>
      </div>

      <div class="item">
        <img src="<?php bloginfo('template_directory'); ?>/images/carousel-bg.png" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          <div class="row equal">
        	<div class="col-lg-3">
        		<img class="person-miniature" src="<?php bloginfo('template_directory'); ?>/images/landing/gumenyk.png" style="margin-top: 60%" >
        	</div>
        	<div class="col-lg-9">
        		<p>Мои пожелания - упорно учиться и усваивать высшее проффесиональное знание, чтобы потом не жалеть о потерянном времени в студенческие годы. К сожалению, мотивацией для такой умственной работы пока является только усилие воли. Дополнительным стимулом для этого может быть участие в научных разработках нашей кафедры. Мои научные разработки формального анализа строя являются передовыми в мире.</p>
            	<div class="righted">
          			<h2>Гюменюк Александр Степанович,</h2>
					<h3>Доцент кафедры</h3> 
				</div>
        	</div>
        	</div>
        </div>
      </div>

      <div class="item">
        <img src="<?php bloginfo('template_directory'); ?>/images/carousel-bg.png" alt="New York" style="width:100%;">
        <div class="carousel-caption">
          <div class="row equal">
        	<div class="col-lg-3">
        				<img class="person-miniature" src="<?php bloginfo('template_directory'); ?>/images/landing/noname1.png" style="margin-top: 60%">
        	</div>
        	<div class="col-lg-9">
        		<p>
				Пожалуй самым ярким моментом была похвала Флоренсова А.Н. после защиты магистерской диссертации — «Поздравляю! Это была лучшая защита».
				</p>
            	<div class="righted">
          			<h2>Алексей Горбунов,</h2>
						<h3>главный специалист отдела экспертизы проектов дирекции защиты информации в ГК «Газпром нефть»,  ООО «ИТСК».</h3>
				</div>
        	</div>
        	</div>
        </div>
      </div>

      <div class="item">
        <img src="<?php bloginfo('template_directory'); ?>/images/carousel-bg.png" alt="New York" style="width:100%;">
        <div class="carousel-caption">
          <div class="row equal">
        	<div class="col-lg-3">
        				<img class="person-miniature" src="<?php bloginfo('template_directory'); ?>/images/landing/gordeev.png" style="margin-top: 60%">
        	</div>
        	<div class="col-lg-9">
        		<p>
				Экзамен по физкультуре. Первым вопрос — «Кровеносная система.» Мне, как настоящему технарю, стало интересно, как все устроено и работает. Через некоторое время я уже сидел с учебником по анатомии и пытался вникнуть во все клапана и вены. Остальные вопросы я читал уже по пути в ВУЗ. Мне редко везло с билетами, но тут я не верил своим глазам — первый билет. После рассказа с иллюстрациями про фазы систолы и диастолы желудочков на остальные вопросы я уже не отвечал.
				</p>
            	<div class="righted">
          			<h2>Гордеев Вячеслав Михайлович,</h2>
						<h3>директор <a href="http://www.a2design.ru">ООО «А2 Дизайн»</a></h3>
				</div>
        	</div>
        	</div>
        </div>
      </div>
      <div class="item">
        <img src="<?php bloginfo('template_directory'); ?>/images/carousel-bg.png" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          <div class="row equal">
        	<div class="col-lg-3">
        	<img class="person-miniature" src="<?php bloginfo('template_directory'); ?>/images/landing/tiunova.png" style="margin-top: 60%">
        	</div>
        	<div class="col-lg-9">
        		<p>Самый запоминающийся момент - наши дружные сдачи курсовых по схемотехнике, которые принимал Шакиров Михаил Федорович. Все, что было связано с подготовкой, опиралось на опыт одногруппников - Бахмутского Юры и Потапова Ильи в вопросах счетчиков и триггеров. У нас была очень дружная группа. Здорово было учиться.</p>
            	<div class="righted">
          			<h2>Тиунова (Лихачёва) Наталья Анатольевна,</h2>
						<h3>Директор по продажам <a href='http://www.technocor.ru'>ИТ-компании «Технокор»</a></h3> 
				</div>
        	</div>
        	</div>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel2" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
			
			</div>	
		</div>
		<div class="info">
			<div class="container">
				<h1>Поступление</h1>
				<h1 class="warning">На направлении 66 <strong>Бюджетных мест</strong></h1>
				<div class="row">
					<div class="col-lg-2 large-icon icon-center" >
						<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
					</div>
					<div class="col-lg-10 lefted">
					<h2>Экзамены, необходимые для поступления</h2>
					<h3><strong>Математика</strong></h3>
					<h3><strong>Физика</strong></h3>
					<h3><strong>Русский язык</strong></h3>
					<h3><span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span><a href="http://www.omgtu.ru/entrant/documents_for_review/statistics_of_past_let/"> Статистика прошлых лет</a></h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 large-icon icon-center" >
						<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>
					</div>
					<div class="col-lg-10 lefted">
					<h2>Абитуриент должен направить в вуз следующие документы</h2>
					<h3>Заявление о приеме в ВУЗ</h3>
					<h3>Оригинал или ксерокопию документов, удостоверяющих личность, гражданство</h3>
					<h3>Оригинал или ксерокопию свидетельства о результатах ЕГЭ</h3>
					<h3>Оригинал или ксерокопию документа государственного образца об образовании</h3>
					
					</div>
				</div>
				<div class="row">
					<div class="col-lg-2 large-icon icon-center" >
						<span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>
					</div>
					<div class="col-lg-10 lefted">
					<h2>Приемная комиссия ОмГТУ</h2>
					<h3>Адрес: Пр. Мира, 11, гл.корпус, фойе, каб П-102</h3>
					<h3>Телефон/факс: +7 (3812) 72-90-55 (многоканальный)</h3>
					<h3>Электронная почта: ivt@omgtu.ru</h3>

					
					</div>
				</div>
				<h3 class="centered"><span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span><a href="http://www.omgtu.ru/entrant/"> Сайт приемной коммисии</a></h1>
			</div>
		</div>
		<div id="maps" style="width: 100%; height: 600px">
			
		</div>
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
											</div>

					<div class="col-lg-3">
						<a href="http://www.omgtu.ru/" class="links">Сайт университета</a>
					</div>
					<div class="col-lg-3">
						<a href="<?php home_url(); ?>/starter" class="links">Сайт кафедры</a>
					</div>
					<div class="col-lg-3">
						<a  href="http://www.omgtu.ru/entrant/where_to_go/index.php" class="links">Сайт приемной комиссии</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript">
  var myMap;
  ymaps.ready(init); // Ожидание загрузки API с сервера Яндекса
  function init () {
    myMap = new ymaps.Map("maps", {
      center: [55.025862, 73.292618], // Координаты центра карты
      zoom: 17 // Zoom
    });
    myMap.balloon.open(myMap.getCenter(),
    {
        contentHeader: '',
        contentBody: '<div id="mapHint">'+'За дополнительной информацией обращаться по адресу:'+'<br>'+' г.Омск, пр. Мира 11, корпус ОмГТУ №1'+'<br>'+'Контактный телефон: 65-24-98'+'</div>',
    }
);
  }

</script>
<script type="<?php bloginfo('template_directory'); ?>/bootstrap/js/bootstrap.min.js"></script>
<script type="<?php bloginfo('template_directory'); ?>/js/carousel.js"></script>
<script>
	$(function() {
  $('#scrollMore').click(function() {
    $("html, body").animate({
      scrollTop:591
    },1000);
  })
  });
	$(function() {
  $('#scrollAbout').click(function() {
    $("html, body").animate({
      scrollTop:1191
    },1000);
  })
  });
	$(function() {
  $('#scrollEdu').click(function() {
    $("html, body").animate({
      scrollTop:1591
    },1000);
  })
  });
	$(function() {
  $('#scrollBase').click(function() {
    $("html, body").animate({
      scrollTop:1791
    },1000);
  })
  });
	$(function() {
  $('#scrollAbit').click(function() {
    $("html, body").animate({
      scrollTop:3291
    },1000);
  })
  });
	$(function() {
  $('#scrollContacts').click(function() {
    $("html, body").animate({
      scrollTop:4591
    },1000);
  })
  })
</script>
</body>
</html>
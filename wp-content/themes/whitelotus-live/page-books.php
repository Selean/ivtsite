<?php get_header();?>

<?php get_sidebar();?>
<section>
	<div class="main-heading">
	<h1><?php the_title(); ?></h1>
</div>
	<?php 

		if($_GET['author'] != NULL)
		{
			$query = new WP_Query( array( 'category_name' => 'books', 
											'byline' => $_GET['author'] ) );
		}
		else
		{
			$query = new WP_Query( array( 'category_name' => 'books' ) );
		}
		
		
		
		if($query->have_posts())
		{
			while ( $query->have_posts() ) {
				$query->the_post();
				$file = get_field('attached_file');
				if($file) {
					echo '<h3><img class="icon" src="'.$file["icon"].'"></img><a href="'.$file['url'].'"> '.get_the_title().'</a></h3>';

					
			    	echo "Преподаватель: ";
			    	the_author_posts_link();
					echo "<hr>";
				}

				
			}
		}
		else {
			echo "Материалов не найдено.";
		}
?>
</section>
<?php get_footer(); ?>
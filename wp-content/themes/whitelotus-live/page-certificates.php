<?php get_header();?>

<?php get_sidebar();?>
<section>
	<div class="main-heading">
		<h1 class="title"><?php the_title(); ?></h1>
	</div>
	
		
	<div>
	<?php 
		
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

        $args = array(
            'post_type' => 'sertificates',
            'order' => 'ASC',
            'posts_per_page' => 9,
            'paged' => $paged,
            'byline' => $_GET['author'] 
        );
		
		if($_GET['author'] != NULL)
		{
			$query = new WP_Query( $args );
			$author = get_term_by( 'slug', $_GET['author'], 'byline' );
			echo '<h3>Поиск по автору: '.$author->name.'</h3>';
		}
		else
		{
			$query = new WP_Query( $args );
		}
		if($query->have_posts()) {
			while ( $query->have_posts() ) {
			$query->the_post();
			$image = get_field('image');
			?>	
					<a rel="nofollow" target="_blank" href="<?php echo $image['url'] ?>">
						<img class='certificate' height="400" src="<?php echo $image['url'] ?>"/>
					</a>
		
		<?php
			}
			?>
		<div>
			<ul class="pagination">
				<li><?php echo get_previous_posts_link( 'Предыдущая страница' ); ?></li>
				<?php 
					$page = 1;
					while($page <= $query->max_num_pages)
					{
						if($page == get_query_var('paged'))
						{
							echo '<li class="active"><a>'.$page.'</a></li>';
						}
						else 
						{
							echo '<li><a href="'.home_url().'/science/certificates/page/'.$page.'">'.$page.'</a></li>';
						}
						$page = $page + 1;
					}
				?>
            	<li><?php echo get_next_posts_link( 'Следующая страница', $query->max_num_pages ); ?></li>
            	
        	</ul>

        </div>
<?php
		}
		else {
			echo "<p>По данному запросу ничего не найдено.</p>";
		}

		?>
	</div>	
		
</section>
<?php get_footer(); ?>


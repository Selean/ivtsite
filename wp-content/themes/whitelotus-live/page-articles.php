<?php get_header();?>

<?php get_sidebar();?>
<section>
<div class="main-heading">
	<h1 class="sec-title"><?php the_title(); ?></h1>
</div>
<div class="post-container">
	<?php 
		if($_GET['author'] != NULL)
		{
			$query = new WP_Query( array( 'category_name' => 'articles', 
											'byline' => $_GET['author'] ) );
		}
		else
		{
			$query = new WP_Query( array( 'category_name' => 'articles' ) );
		}
		
		
		while ( $query->have_posts() ) {
			$query->the_post();
			?>
			<div class="post-block">
				<h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		    
			
				<?php the_excerpt();?>
				<?php the_author_posts_link(); ?>
				
			</div>
			<?php
		}
?>
</div>
</section>
<?php get_footer(); ?>
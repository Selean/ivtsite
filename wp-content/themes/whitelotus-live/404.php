<?php get_header(); ?>
<?php get_sidebar(); ?>
<?php get_sidebar(); ?>
<section>
	<div class='block-404'>
		<img src="<?php bloginfo('template_directory'); ?>/images/404.jpg">
		<h1 class='large-title'>Страница не найдена</h1>
	</div>
	
	<h3>Запрашиваемая вами страница отсутствует. </h3>
	<h3>Возможно, она была удалена, или Вы набрали неправильный адрес. </h3>
</section>
<?php get_footer(); ?>